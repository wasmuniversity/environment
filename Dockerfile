FROM gitpod/openvscode-server:latest

# Run it: docker run -it --init -p 3000:3000 -p 8080:8080 -v "$(pwd):/home/workspace" openvscode-arm64-go-rust:latest

# Settings
ARG GODISTRIB="go1.17.8.linux-amd64.tar.gz"
#ARG GODISTRIB="go1.17.7.linux-arm64.tar.gz"
ARG BATVERSION="v0.19.0"
ARG BATDISTRIB="bat_0.19.0_amd64.deb"
#ARG BATDISTRIB="bat_0.19.0_arm64.deb"

# to get permissions to install packages and such
USER root
# the installation process for software needed 
RUN apt-get update && \
    apt-get install curl nano unzip -y && \
    apt-get install build-essential -y && \
    apt-get install httpie -y 


# GoLang
RUN wget https://go.dev/dl/$GODISTRIB && \
    tar -C /usr/local -xzf $GODISTRIB && \
    rm $GODISTRIB

# 🖐️ there is no tinygo version for arm
RUN wget https://github.com/tinygo-org/tinygo/releases/download/v0.21.0/tinygo_0.21.0_amd64.deb && \
    sudo dpkg -i tinygo_0.21.0_amd64.deb && \
    rm tinygo_0.21.0_amd64.deb

RUN echo "export PATH=$PATH:/usr/local/go/bin" >> /home/openvscode-server/.bashrc

# RustLang
RUN curl https://sh.rustup.rs -sSf | sudo RUSTUP_HOME=/home/openvscode-server/.rustup CARGO_HOME=/home/openvscode-server/.cargo sh -s -- -y

RUN echo 'export RUSTUP_HOME=/home/openvscode-server/.rustup' >> /home/openvscode-server/.bashrc
RUN echo 'export CARGO_HOME=/home/openvscode-server/.cargo' >> /home/openvscode-server/.bashrc
RUN echo 'export PATH=$PATH:$CARGO_HOME/bin' >> /home/openvscode-server/.bashrc
# fix $HOME/.cargo permissions after rustup installation (it ends up being owned as root)
RUN ["/bin/bash", "-c", "sudo chown -R openvscode-server /home/openvscode-server/.cargo/"]

# to restore permissions for the web interface
USER openvscode-server 

#SHELL ["/bin/bash", "-c"]
# install exa
RUN export RUSTUP_HOME=/home/openvscode-server/.rustup && \ 
    export CARGO_HOME=/home/openvscode-server/.cargo && \
    export PATH=$PATH:$CARGO_HOME/bin && \
    cargo install exa

# install bat
RUN wget https://github.com/sharkdp/bat/releases/download/$BATVERSION/$BATDISTRIB && \
    sudo dpkg -i $BATDISTRIB && \
    rm $BATDISTRIB

# NodeJs
RUN curl -sL https://deb.nodesource.com/setup_17.x -o nodesource_setup.sh && \
    sudo bash nodesource_setup.sh && \
    sudo apt-get install nodejs -y


