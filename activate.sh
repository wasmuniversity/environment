#!/bin/bash
git config --global user.email "pcharriere@gitlab.com"
git config --global user.name "@k33g"

eval $(ssh-agent -s)
ssh-add /home/workspace/.openvscode-server/.ssh/id_rsa
ssh -Tvvv git@gitlab.com

git config core.sshCommand "ssh -o IdentitiesOnly=yes -i /home/workspace/.openvscode-server/.ssh/id_rsa -F /dev/null"
