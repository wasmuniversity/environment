# OpenVSCode Server

This project use https://github.com/gitpod-io/openvscode-server

- The user is `openvscode-server` (tools are installed in the home of this user, or root).
- The path of the project (the volume) is mapped on `workspace`

```bash
..
├── openvscode-server
└── workspace
   ├── .bashrc
   ├── .vscode
   │  └── settings.json
   └── README.md
```

- You need to add a `workspace/.bashrc` file with this content `source /home/openvscode-server/.bashrc`. This will be run every time you open a terminal.
- You need to add a `workspace/.vscode/settings.json` with this content

## Golang Settings

```json
{
    "workbench.iconTheme": "vs-seti",
    "workbench.colorTheme": "Monokai",
    "terminal.integrated.defaultProfile.linux": "bash",
    "terminal.integrated.cwd": "/home/workspace",
    "go.toolsEnvVars": {
        "GOOS": "js",
        "GOARCH": "wasm",
    },
    "gopls": {
      "experimentalWorkspaceModule": true,
    },
}
```

You need to add a `workspace/.vscode/extensions.json` with this content

```json
{
    "recommendations": [
        "golang.Go"
    ]
}
```

wip: go settings
- https://www.jetbrains.com/help/go/configuring-goroot-and-gopath.html

```json
    "go.goroot": "/home/gitpod/.gvm/gos/go1.17.5",
    "go.gopath": "/home/gitpod/.gvm/pkgsets/go1.17.5/global",
```



