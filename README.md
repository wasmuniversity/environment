# Wasm environment

## How to use this project

- `git clone` this project `git clone https://gitlab.com/wasmuniversity/environment.git`
- run the below commands:
  ```bash
  cd environment
  docker run -it --init -p 3000:3000 -p 8080:8080 -p 8081:8081 -v "$(pwd):/home/workspace" k33g/openvscode-amd64-go-rust:0.0.0
  ```
- open this URL: http://localhost:3000
  ![ovscode-01](./pictures/ovscode-01.png)
- click on the button <kbd>Open Folder</kbd> (and chose `/home/workspace/`)
  ![ovscode-02](./pictures/ovscode-02.png)
- 🎉 you can now experiment
  ![ovscode-03](./pictures/ovscode-03.png)

> 🖐️ you can build your own image: 👀 look at `Dockerfile`
> ```bash
> app_name="openvscode-amd64-go-rust"
> tag="0.0.0"
> docker build -t ${app_name} .
> docker tag ${app_name} ${registry}/${app_name}:${tag}
> ```

## SSH Keys

If you need to create SSH Keys to commit your work:

```bash
mkdir /home/workspace/.openvscode-server/.ssh
ssh-keygen -t rsa -b 2048 -C "ssh key for openvscode server" -f /home/workspace/.openvscode-server/.ssh/id_rsa 
chmod 600 /home/workspace/.openvscode-server/.ssh
cat /home/workspace/.openvscode-server/.ssh/id_rsa.pub
```
> 🖐️ files will be generated to `/home/workspace/.openvscode-server/.ssh/id_rsa` and persisted locally on your computer

> 🖐️ add the public keys to your GitLab/GitHub/Bitbucket settings

### Identify yourself on the Git Server

> 🖐️ do this at every restart of the container

> 🖐️ the below commands are used with GitLab, adapt these commands if you use GitHub or Bitbucket

Tell who you are:
```bash
git config --global user.email "bob.morane@gitlab.com"
git config --global user.name "@bob"
```

```bash
eval $(ssh-agent -s)
ssh-add /home/workspace/.openvscode-server/.ssh/id_rsa
ssh -Tvvv git@gitlab.com

git config core.sshCommand "ssh -o IdentitiesOnly=yes -i /home/workspace/.openvscode-server/.ssh/id_rsa -F /dev/null"
```

Refs:
- https://docs.gitlab.com/ee/ssh/


