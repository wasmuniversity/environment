const fs = require('fs/promises')

require("./wasm_exec")

function runWasm(wasmFile, args) {
  const go = new Go()
  // TinyGo hack
  //go.importObject.env["syscall/js.finalizeRef"] = () => {}

  return new Promise((resolve, reject) => {
    WebAssembly.instantiate(wasmFile, go.importObject)
    .then(result => {
      if(args) go.argv = args
      go.run(result.instance) 
      resolve(result.instance)
    })
    .catch(error => {
      reject(error)
    })
  })
}

fs.readFile('./main.wasm')
.then(wasmFile => runWasm(wasmFile))
.then(wasm => {
  console.log(Hello("Bob"))
  //console.log(HeyGoogle())
  HeyUrl("https://google.com").then(data => console.log(data)).catch(err => console.log(err))
})
.catch(error => {
  console.log("ouch", error)
}) 




