# Suborbital

🚧 this is a work in progress


## Install subo CLI

### Amd version

```bash
wget https://github.com/suborbital/subo/releases/download/v0.3.2/subo-v0.3.2-linux-amd64.tar.gz
mkdir subo
tar -C subo -xzf subo-v0.3.2-linux-amd64.tar.gz 
rm subo-v0.3.2-linux-amd64.tar.gz
```

### Arm version

```bash
wget https://github.com/suborbital/subo/releases/download/v0.3.2/subo-v0.3.2-linux-arm64.tar.gz
mkdir subo
tar -C subo -xzf subo-v0.3.2-linux-arm64.tar.gz 
rm subo-v0.3.2-linux-arm64.tar.gz
```

### Use Subo

```bash
./subo/subo create runnable hello
./subo/subo build --native hello
```

## Sat

## Atmo

